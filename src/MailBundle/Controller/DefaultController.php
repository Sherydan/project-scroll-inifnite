<?php

namespace MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route(
     *        "/mail/{last}", 
     *        name="mail_homepage",
     *        defaults={"last" = null},
     *        requirements = { "last" = "\d+" },
     *        options = { "expose" = true }
     *       )
     */
    public function indexAction(Request $request, $last = null)
    {
        
        $em = $this->getDoctrine()->getManager();
        $mails = $em->getRepository('MailBundle:Message')->getListeMessage($last);

        if($request->isXmlHttpRequest()) {
            return $this->render('MailBundle:Default:liste.html.twig', array(
                    'listeEmails' => $mails,
        ));
        }

        return $this->render('MailBundle:Default:index.html.twig', array(
                    'listeEmails' => $mails,
        ));
    }
    
}
