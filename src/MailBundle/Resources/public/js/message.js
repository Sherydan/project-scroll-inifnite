function infiniteScroll(){
    $(function(){
                          
        $(window).scroll(function(){
            var lastBlock              = $('.div-message').find('.block-message:last');       
            var offset                 = lastBlock.offset();
            var load                   = false;
            var messageLoadposition    = ($(window).height() / 2) + 10;
            if((offset.top - $(window).height() <= $(window).scrollTop()) && load==false){
                load          = true;
                var lastId    = lastBlock.data('message-id');
                $('body').css('overflow','hidden');
                $('.load-more').removeClass('hide');
                $('.loading-msg').css({'position':'absolute', 'bottom': messageLoadposition+'px'});
                $.ajax({
                    url: Routing.generate('mail_homepage', { 'last': lastId }),
                    type: 'get',
                    dataType: 'html' ,
                    success: function(data) {
                        $('.div-message').append(data); 
                        $('body').css('overflow','auto');
                        $('.load-more').addClass('hide');                      
                        load = false;
                    }
                });
            }
        });
  });
}

$(function(){ infiniteScroll.apply(document);});