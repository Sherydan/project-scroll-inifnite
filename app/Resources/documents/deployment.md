[<< Return README](../../../README.md)

# Deployment instructions

## Install dependencies

- Launch ```composer install```

## Create database

- Launch ```app/console doctrine:database:create```
- Launch ```app/console doctrine:schema:update --dump-sql --force```

## Install Assets

- Launch ```app/console assets:install```

## Generate Routing in js

- Launch ```php app/console fos:js-routing:dump```

##  Load data fixtures to database.

- Launch ```php app/console doctrine:fixtures:load```

## You should now have access to this new project website

- Open ```http://localhost/project-scroll-inifnite/web/app_dev.php/mail``` on your browser

